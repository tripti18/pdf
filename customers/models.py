from django.db import models

# Create your models here.

class Customer(models.Model):
    partname = models.CharField(max_length=122)
    material = models.CharField(max_length=122)
    process = models.CharField(max_length=122)
    quantity = models.CharField(max_length=122)
    rate = models.CharField(max_length=122)
    total = models.CharField(max_length=122)
    date = models.DateTimeField()

    