from django.shortcuts import render, get_object_or_404
from datetime import datetime

from django.http import HttpResponse
from django.template.loader import get_template
from xhtml2pdf import pisa
from django.views.generic import ListView
from .models import Customer


# Create your views here.


def customer(request):
    # return HttpResponse("this is home page")
    if request.method == "POST":
        partname = request.POST.get('partname')
        material = request.POST.get('material')
        process = request.POST.get('process')
        quantity = request.POST.get('quantity')
        rate = request.POST.get('rate')
        total = request.POST.get('total')
        customer = Customer(partname=partname, material=material, process=process, quantity=quantity, rate=rate, total=total, date=datetime.today())
        customer.save()
        
    return render(request, ('main.html'))


class CustomerListView(ListView):
   model = Customer
   template_name = 'customers/main.html'

def customer_render_pdf_view(request, *args, **kwargs):
    pk = kwargs.get('pk')
    customer = get_object_or_404(Customer, pk=pk)
    template_path = 'customers/pdf2.html'
    context = {'customer': customer}
    # Create a Django response object, and specify content_type as pdf
    response = HttpResponse(content_type='application/pdf')

   #  if download:
   #  response['Content-Disposition'] = 'attachment; filename="report.pdf"'

   #if want to display in browser:
    response['Content-Disposition'] = 'filename="report.pdf"'

    # find the template and render it.
    template = get_template(template_path)
    html = template.render(context)

    # create a pdf
    pisa_status = pisa.CreatePDF(
       html, dest=response)
    # if error then show some funny view
    if pisa_status.err:
       return HttpResponse('We had some errors <pre>' + html + '</pre>')
    return response



def render_pdf_view(request):
    template_path = 'customers/pdf1.html'
    context = {'myvar': 'this is your template context'}
    # Create a Django response object, and specify content_type as pdf
    response = HttpResponse(content_type='application/pdf')

   #  if download:
   #  response['Content-Disposition'] = 'attachment; filename="report.pdf"'

   #if want to display in browser:
    response['Content-Disposition'] = 'filename="report.pdf"'

    # find the template and render it.
    template = get_template(template_path)
    html = template.render(context)

    # create a pdf
    pisa_status = pisa.CreatePDF(
       html, dest=response)
    # if error then show some funny view
    if pisa_status.err:
       return HttpResponse('We had some errors <pre>' + html + '</pre>')
    return response